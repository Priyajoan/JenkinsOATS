package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
//import wdMethods.ProjectMethods;
import pages.MyHomePage;
public class TSC002_EditLead extends MyHomePage {
	
	@BeforeClass
	public void setData(){
		testCaseName = "TC002_EditLead";
		testCaseDescription ="Edit a lead";
		category = "Smoke";
		author= "Priya";
		dataSheetName="TC002";
	}

	
	
	@Test(dataProvider="fetchData")
	public  void editLead(String phnNumber, String updateCname)  throws InterruptedException {
		
		new MyHomePage()
		.clickLeads()
		.clickFindLeads()
		.clickLinkPhoneNumber()
		.typePhoneNumber(phnNumber)
		.clickFindLeads()
		.clickFirstSearchResult()
		.clickEdit()
		.updateCompanyName(updateCname)
		.clickUpdate();
		
		
		
		
		/*click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Find Leads"));
		click(locateElement("xpath", "//span[text()='Phone']"));
		type(locateElement("name", "phoneNumber"),phnNumber);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(2000);
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		click(locateElement("linktext", "Edit"));
		type(locateElement("id", "updateLeadForm_companyName"),updateCname);
		click(locateElement("class", "smallSubmit"));*/
		
	
	}
}
