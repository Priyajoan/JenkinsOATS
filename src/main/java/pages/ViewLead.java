package pages;
import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods{
	public OpenTaps clickEdit()
	{
		WebElement eleEdit = locateElement("linktext", "Edit");
		click(eleEdit);
		return new OpenTaps();
	}

}
