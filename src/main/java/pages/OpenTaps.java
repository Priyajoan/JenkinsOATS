package pages;
import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class OpenTaps extends ProjectMethods {
	public OpenTaps updateCompanyName(String updateCname) {
		WebElement elecompanyName = locateElement("id", "updateLeadForm_companyName");
		type(elecompanyName,updateCname);
		return this;
	}
	
	public ViewLead clickUpdate()
	{
		WebElement eleUpdate = locateElement("class", "smallSubmit");
		click(eleUpdate);
		return new ViewLead();
	}

}
