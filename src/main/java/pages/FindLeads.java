package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	
	public FindLeads() {
		PageFactory.initElements(driver,this);
	}
	
	@CacheLookup
	@FindBy(xpath="//span[text()='Phone']")
	WebElement eleLinkPhoneNumber;
	
	@CacheLookup
	@FindBy(name="phoneNumber")
	WebElement elePhoneNumber;
	
	@CacheLookup
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleFindLeads;
	
	@CacheLookup
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleFirstInTable;
	
	//@FindAll
	
	public FindLeads clickLinkPhoneNumber()
	{
		//WebElement eleLinkPhoneNumber = locateElement("xpath", "//span[text()='Phone']");
		click(eleLinkPhoneNumber);
		return this;
	}
	public FindLeads typePhoneNumber(String PhNo)
	{
	//	WebElement elePhoneNumber  = locateElement("name", "phoneNumber");
		type(elePhoneNumber,PhNo);
		return this;
	}
	public FindLeads clickFindLeads() throws InterruptedException
	{
		//WebElement eleFindLeads= locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		Thread.sleep(3000);
		return this;
	}
	
	public ViewLead clickFirstSearchResult()
	{
	//	WebElement eleFirstInTable = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFirstInTable);
		return new ViewLead();
	}
	
	
}
